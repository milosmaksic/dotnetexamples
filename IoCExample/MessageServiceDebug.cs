﻿namespace IoCExample
{
    public class MessageServiceDebug : IMessageService
    {
        public string PrintMessage()
        {
            return "This is a message from DEBUG.";
        }
    }
}