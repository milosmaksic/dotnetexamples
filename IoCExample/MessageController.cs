﻿using System;

namespace IoCExample
{
    public class MessageController
    {
        private IMessageService _messageService;

        public MessageController() { }

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public IMessageService MessageService
        {
            set { _messageService = value; }
        }

        public void DisplayMessage()
        {
            Console.WriteLine(_messageService.PrintMessage());
            Console.ReadLine();
        }

        public void DisplayMessageInMethod(IMessageService messageService)
        {
            Console.WriteLine(messageService.PrintMessage());
            Console.ReadLine();
        }
    }
}