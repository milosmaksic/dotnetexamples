﻿namespace IoCExample
{
    public class MessageServiceRelease : IMessageService
    {
        public string PrintMessage()
        {
            return "This is a message from RELEASE.";
        }
    }
}