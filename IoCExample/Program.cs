﻿using System;

namespace IoCExample
{
    class Program
    {
        private static MessageController _showMessageController;
        private static IMessageService _messageService;

        static void Main(string[] args)
        {
            Configure();
            ConstructorInjection(); // Uncomment this line to use constructor injection
            //PropertyInjection(); // Uncomment this line to use property injection
            //MethodInjection(); // Uncomment this line to use method injection
        }

        // Recommended when we need to use in more methods in class
        private static void ConstructorInjection()
        {
            Console.WriteLine("----- Constructor Injection -----");
            _showMessageController = new MessageController(_messageService);
            _showMessageController.DisplayMessage();
        }

        // Recommended when using is optional
        private static void PropertyInjection()
        {
            Console.WriteLine("----- Property Injection -----");
            _showMessageController = new MessageController();
            _showMessageController.MessageService = _messageService;
            _showMessageController.DisplayMessage();
        }

        // Recommended when we need to use it only in specific method
        private static void MethodInjection()
        {
            Console.WriteLine("----- Method Injection -----");
            _showMessageController = new MessageController();
            _showMessageController.DisplayMessageInMethod(_messageService);
        }

        private static void Configure()
        {
#if DEBUG
            _messageService = new MessageServiceDebug();
#else
            _messageService = new MessageServiceRelease();
#endif
        }
    }
}
